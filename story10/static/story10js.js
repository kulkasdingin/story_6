$(document).on('submit','#user_form',function(e){
    e.preventDefault();

    $.ajax({
        type:'POST',
        url:'/ajax/create/',
        data:{
            name:$('#name').val(),
            email:$('#email').val(),
            password:$('#password').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success : function(){
            alert("Subscribed!!");
        }
    });
});

$(".input").change(function(){
    $.ajax({
        url:'/ajax/validate/',
        data:{
             'email' : $('#email').val(),
             'name' : $('#name').val(),
             'password' : $('#password').val(),
             'confirm_password' : $('#password_c').val()
        },

        dataType:'json',
        success: function(data){
            if(data.is_taken){
                $('#error_email').text("Email telah digunakan");
            }
            else{
                $('#error_email').text("");
            }

            if(data.is_equal){
                $('#error_password_c').text("");
            }
            else{
                $('#error_password_c').text("Password tidak sama");
            }

            if(data.is_submitable){
                $('#register').prop('disabled',false);
            }
            else{
                $('#register').prop('disabled',true);
            }

            console.log(data.is_submitable);
        }
    });

});