from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from story10.models import User

def index10(request):
    return render(request,'daftar.html')

def subscribe(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        user = User(
            name = name,
            email = email,
            password = password
        )
        user.save()

        return HttpResponse('')

def validate_form(request):
    name = request.GET.get('name', None)
    email = request.GET.get('email', None)
    password = request.GET.get('password', None)
    password_confirmation = request.GET.get('confirm_password', None)

    v_email = User.objects.filter(email=email).exists()
    v_password = True if (password == password_confirmation) else False
    v_all = not v_email and email and name and password and password_confirmation and v_password
    # v_all = True

    data = {
        'is_taken' : v_email,
        'is_equal' : v_password,
        'is_submitable' : v_all
    }

    return JsonResponse(data)

# Create your views here.
