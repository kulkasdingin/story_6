from django.contrib import admin
from .models import User

class UserModelAdmin(admin.ModelAdmin):
    list_display = ["name","email"]

    class Meta:
        model = User


admin.site.register(User, UserModelAdmin)
# Register your models here.
