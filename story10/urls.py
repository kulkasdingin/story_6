from django.urls import path

from . import views

urlpatterns = [
    path('',views.index10,name='index'),
    path('ajax/create/',views.subscribe , name='subscribe'),
    path('ajax/validate/', views.validate_form, name='validate'),
]