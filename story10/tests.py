from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .views import index10, subscribe, validate_form
from .models import User

class Story10Test(TestCase):
    def test_story_10_url_is_exist(self):
        response= Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_story_10_using_index_func(self):
        found = resolve('/story10/')
        self.assertEqual(found.func, index10)

    def test_story_10_using_subscribe_func(self):
        found = resolve('/story10/ajax/create/')
        self.assertEqual(found.func, subscribe)

    def test_story_10_using_validate_form_func(self):
        found = resolve('/story10/ajax/validate/')
        self.assertEqual(found.func, validate_form)

    def test_hello_exist(self):
        request = HttpRequest()
        response = index10(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Subscribe', html_response)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/story10/ajax/create/',
                                    data={'name': 'nama test',
                                          'email':'email@test',
                                          'password':'test',
                                          })
        counting_all_available_name = User.objects.all().count()

        self.assertEqual(counting_all_available_name, 1)
        self.assertEqual(response.status_code, 200)
# Create your tests here.
