from django.test import TestCase
from django.test import Client
from django.urls import resolve
# from django.http import HttpRequest


from .views import story9

class Story9Test(TestCase):
    def test_story_9_url_is_exist(self):
        response= Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_story9_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)



# Create your tests here.
