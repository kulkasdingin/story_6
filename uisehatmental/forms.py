from django import forms

class NameForm(forms.Form):
    error_messages = {
        'required': 'Please fill this forms',
        'invalid': 'is not valid',
    }

    attrs = {
        'class': 'form-control'
    }
    text = forms.CharField(label='Masukan nama', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))