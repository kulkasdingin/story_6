from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

import time

from .views import story7 as index, index as indexx
from .models import Nama
from .forms import NameForm


class Story6Test(TestCase):
    def test_story_6_url_is_exist(self):
        response= Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)

    def test_story_6_using_indexx_func(self):
        found = resolve('/index/')
        self.assertEqual(found.func, indexx)

    def test_model_can_make_new_name(self):
        new_name = Nama.objects.create(text="Zaenudeen")
        counting_all_available_name = Nama.objects.all().count()
        self.assertEqual(counting_all_available_name,1)

    def test_form_validation_for_blank_items(self):
        form = NameForm(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            ["This field is required."]
        )

    def test_hello_exist(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apakabar!', html_response)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/story7/add_name/',
                                    data={'text': 'name update'})
        counting_all_available_name = Nama.objects.all().count()

        self.assertEqual(counting_all_available_name, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/uisehatmental/')

        new_response = self.client.get('/story7/')
        html_response = new_response.content.decode('utf8')

        self.assertIn('name update', html_response)

# class Story6FuncTest(LiveServerTestCase):
#     def setUp(self):
#         # firefox_option = Options()
#         # firefox_option.add_argument("--headless")
#         self.selenium = webdriver.Firefox()
#         super(Story6FuncTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FuncTest, self).tearDown()
#
#     def test_input_coba(self):
#         selenium = self.selenium
#
#         selenium.get('http://localhost:8000/')# self.live_server_url
#
#         form = selenium.find_element_by_id('id_text')
#         submit = selenium.find_element_by_id('submit')
#
#         form.send_keys('Coba Coba')
#         submit.send_keys(Keys.RETURN)
#
#         self.assertIn('Coba Coba', selenium.page_source)
#
#     def test_layout(self):
#         selenium=self.selenium
#
#         selenium.get('http://localhost:8000/profil/')
#
#         time.sleep(15)
#         header = selenium.find_element_by_id('header').text
#         self.assertIn('Daftar', header)
#
#         body = selenium.find_element_by_id('tentang saya').text
#         self.assertIn('Tentang Saya', body)
#
#     def test_css(self):
#         selenium=self.selenium
#
#         selenium.get('http://localhost:8000/profil/')
#         header = selenium.find_element_by_id('header')
#         headercss = header.value_of_css_property('letter-spacing')
#
#         self.assertEqual('1px',headercss)
#
#         perkenalan = selenium.find_element_by_id('Perkenalan')
#         perkenalancss = perkenalan.value_of_css_property('text-align')
#
#         self.assertEqual('left',perkenalancss)



# Create your tests here.
