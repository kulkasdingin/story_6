from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .forms import NameForm
from .models import Nama

# Create your views here.

def story7(request):
    all_name = Nama.objects.all()
    form = NameForm
    return render(request, 'index.html', {'Nama': all_name, 'form': form})

def index(request):
    return render(request, 'profil.html',{})


def add_name(request):
    form = NameForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        text = request.POST.get('text', False)
        text = Nama(text=text)
        text.save()
        return redirect(index)
    else:
        return HttpResponseRedirect('/index/')

