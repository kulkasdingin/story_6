"""mentalhealthmatters URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

from uisehatmental.views import index, add_name
from story10.views import subscribe, validate_form

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('uisehatmental.urls')),
    path('story9/', include('story9.urls')),
    path('story10/', include('story10.urls')),

    url('add_name/', add_name, name='add_name'),
    url(r'^index/$',index, name='index'),
    url('ajax/create/',subscribe, name='subscribe'),
    url(r'^ajax/validate/$',validate_form, name='validate'),
    url('uisehatmental/',index, name='uisehatmental'),
]
